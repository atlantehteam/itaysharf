<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="xoxo">
            
            
             <?php 
 
				// check for rows (parent repeater)
				if( get_field('more-pages') ): ?>
                <div class="sidebar-title"><?php the_field('more-pages-title');?></div>
                <ul class="more-pages">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('more-pages') ): ?>
						<li class="item">
                    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title' ); ?></a>
                    
                     
                     						 						
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
                    </ul>
									<?php endif; // if( get_field('disease') ): ?>
                                    
                                    
                      <div class="side-img landing hide-mobile"> 
   				 <div class="sidebar-title">איתי שרף בתקשורת</div>
					<a href="/wp-content/themes/itaysharf/video/itaysharf.mp4" class="fancybox-youtube" onclick="javascript:ga('send', 'event', 'Video', 'play', 'interview-landing');">
			         <img src="https://www.itaysharf.com/wp-content/uploads/2016/06/video-cover4.jpg" alt="מרפאת איתי שרף"></a></div>



            
            <?php if (!is_page(19)) :?>
            <div class="inner-form">
	<div class="form-title">יצירת קשר</div>
         <?php echo do_shortcode('[contact-form-7 id="6597" title="new-form"]'); ?>
         </div>
         <?php endif ?>
         
  <div class="patient-stories">
    <div class="sidebar-title"><?php the_field('patients-title', 'options'); ?> <a href="<?php the_field('patients-link', 'options'); ?>" class="btn">לכל הסרטונים</a></div>
    <?php $image = wp_get_attachment_image_src( get_field('patients-clip', 'options'), full ); ?>
   <a href="<?php the_field('video-story', 'options'); ?>&autoplay=1&rel=0" class="fancybox-youtube">
    <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('patients-clip', 'options'))?>" width="331" height="93"/></a>
</div><!-- #patients -->

<div class="side-img landing hide-desktop"> 
   				 <div class="sidebar-title">איתי שרף בתקשורת</div>
					<a href="/wp-content/themes/itaysharf/video/itaysharf.mp4" class="fancybox-youtube" onclick="javascript:ga('send', 'event', 'Video', 'play', 'interview-landing');">
			         <img src="https://www.itaysharf.com/wp-content/uploads/2016/06/video-cover4.jpg" alt="מרפאת איתי שרף"></a></div>


<div class="about">
 <div class="sidebar-title"><?php the_field('about-title', 'options'); ?></div>
  <?php $image = wp_get_attachment_image_src( get_field('about-image', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('about-image', 'options'))?>" width="331" height="114"/></a>
  <div class="about-content">
  <?php the_field('about-content', 'options'); ?>
  <a href="<?php the_field('about-link', 'options'); ?>" class="more">אודות הקליניקה</a>
  </div>
</div>

<div class="book">
 <div class="sidebar-title"><?php the_field('book-title', 'options'); ?></div>
  <?php $image = wp_get_attachment_image_src( get_field('book-image', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('book-image', 'options'))?>" width="108" /></a>
  <div class="book-content">
  <?php the_field('book-content', 'options'); ?>
  <a href="<?php the_field('book-link', 'options'); ?>" class="more">לפרטים נוספים ולרכישה</a>
  </div>
</div>





<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

		

		<?php endif; // end primary widget area ?>
			</ul>
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>
