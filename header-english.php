<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' /> 
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/ltr.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
</head>

<body <?php body_class(); ?>>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/ltr.css" />
 <script >
jQuery(document).ready(function($){
$("#tabs").tabs();
});

jQuery(document).ready(function($){
$("#tabs-adv").tabs({collapsible:true, active:false});
 $( ".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *" )
.removeClass( "ui-corner-all ui-corner-top" )
.addClass( "ui-corner-bottom" );
// move the nav to the bottom
$( ".tabs-bottom .ui-tabs-nav" ).appendTo( ".tabs-bottom" );
});


</script>

<script  > 
jQuery(document).ready(function($){
 $(".video_wrapper").on('click',"a", function(event) {
  event.preventDefault();
  event.stopImmediatePropagation();
  event.stopPropagation();
   
 $(".video_case iframe").prop("src", $(event.currentTarget).attr("href"));

});
});
 </script>
 
 <script >
  jQuery(document).ready(function ($) {
    $(".menu-header li.launcher").mouseover(

    function () {
        $("div.megamenu").fadeIn('fast');
		$(".launcher").addClass('selected');
    });
    $(".menu-header li.launcher, div.megamenu").mouseleave(function () {
        $("div.megamenu").fadeOut('fast');
			$(".launcher").removeClass('selected');
	
    });
    $("div.megamenu").mouseover(function () {
        $(this).stop(true, true).show();
			$(".launcher").addClass('selected');
    });
});
    </script>

<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div id="branding" role="banner">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'div' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><strong>ITAY SHARF</strong> OPHTHALMOLOGY OF CHINESE MEDICINE</a>
					</span>
				</<?php echo $heading_tag; ?>>
			
            <?php if (is_home()) {?>
            <div class="hp-header-details">
            <img src="<?php bloginfo('template_directory'); ?>/images/header-80-tagline.png" alt="איתי שרף - רפואת עיניים סינית"/>
            </div>
            
            <?php } else { ?>
                        <div class="header-details">
                        <a href="<?php echo bloginfo('url');?>" class="heb">עברית</a>
 Call now: +972-77-2004961</div>
                 <?php } ?>
         

				
			</div><!-- #branding -->

			
		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main">
