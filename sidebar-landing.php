<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="xoxo">
			<?php 
 
				// check for rows (parent repeater)
				if( get_field('more-pages') ): ?>
                <h5><?php the_field('more-pages-title');?></h5>
                <ul class="more-pages">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('more-pages') ): ?>
						<li class="item">
                    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title' ); ?></a>
                    
                     
                     						 						
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
                    </ul>
									<?php endif; // if( get_field('disease') ): ?>
					

            <div class="side-img">  
			<?php $image = wp_get_attachment_image_src( get_field('side-img'), full ); ?>
        <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('side-img'))?>" style="border:0px;" /></div>
            
            <div class="inner-form landing">
 <h5>למידע נוסף צרו קשר</h5>
         <?php echo do_shortcode('[contact-form-7 id="84" title="landing-contact"]'); ?>
		<div class="side-img landing"> 
   				 <h5>איתי שרף בתקשורת</h5>
					<a href="/wp-content/themes/itaysharf/video/itaysharf.mp4" class="fancybox-youtube" onclick="javascript:ga('send', 'event', 'Video', 'play', 'interview-landing');">
			         <img src="https://www.itaysharf.com/wp-content/uploads/2016/06/video-cover4.jpg" alt="מרפאת איתי שרף"></a></div>
  
  				
         </div>
         

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

		

		<?php endif; // end primary widget area ?>
			</ul>
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
				
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>