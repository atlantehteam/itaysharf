<?php
/**
 * Template Name: Homepage
 *
 * Homepage Template
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>


		<div id="hp-container">
			<div id="hp-content" role="main">
<div class="hp-top">
<?php $image = wp_get_attachment_image_src( get_field('hp-top-image', 'options'), full ); ?>
<img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('hp-top-image', 'options'))?>" width="360"/>

<div class="hp-top-content"><h1><?php the_field('title', 'options'); ?></h1>
<p><?php the_field('content', 'options'); ?></p>
</div>
</div>
<ul class="advantages">
<li><a href="#">טיפול יעיל ונטול תופעות לוואי</a></li>
<li><a href="#">השלמת הטיפול המערבי במחלות עיניים</a></li>
<li><a href="#">השילוב המרפא לטיפול במחלות עיניים</a></li>
</ul>

 <div id="tabs-adv" class="tabs-bottom">
 <div class="tabs-spacer"></div>
<!-- Using the jquery UI tabs together with ACF plugin. Don't forget to enqeue the jquery ui tabs in the functions.php file -->
	<ul>
	
			<?php if( get_field('tabs-adv', 'options') ): ?>
            
            <?php while (has_sub_field('tabs-adv', 'options') ): $count++ ?>
				<li><h2><a title="<?php the_sub_field('tab_name-adv', 'options'); ?>" href="#tab-<?php echo $count ?>"><?php the_sub_field('tab_name-adv', 'options'); ?></a><span class="arr"></span></h2></li>
                <?php endwhile; ?>
			<?php endif; ?>
	
	</ul>
	
		<?php if( get_field('tabs-adv', 'options') ): ?>
         <?php while (has_sub_field('tabs-adv', 'options') ): $count2++ ?>
			<div id="tab-<?php echo $count2 ?>">
			   <?php $image = wp_get_attachment_image_src( get_sub_field('adv-image', 'options'), full ); ?>
               <div class="img-cont">
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_sub_field('adv-image', 'options'))?>" width="330" height="178"/></div><?php the_sub_field('tab_content-adv', 'options'); ?>
  <a href="<?php the_sub_field('link-adv', 'options'); ?>" class="more">למידע נוסף</a>
           
          
            </div>
		
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<div class="hp-quads">
 <div class="hp-title"><?php the_field('disease-title', 'options'); ?> <a href="<?php the_field('disease-link', 'options'); ?>" class="all">לכל המחלות</a></div>
 
    <?php 
 
				// check for rows (parent repeater)
				if( get_field('disease', 'options') ): ?>
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('disease', 'options') ): ?>
						<div class="item">
                     <h2 > <a href="<?php the_sub_field('link', 'options'); ?>"><?php the_sub_field('title', 'options'); ?></a></h2>
                    
<div class="sub-title"><?php the_sub_field('content', 'options'); ?> <a href="<?php the_sub_field('link', 'options'); ?>" class="more">למידע נוסף</a></div>
                     
                     						 						
						</div>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
									<?php endif; // if( get_field('disease') ): ?>
                                    
 
 
 <div class="hp-contact">
		 <div class="contact-info">
       <!--  <h4>יצירת קשר</h4>-->
             <div class="contact-details">
                 <strong>הודעה חשובה</strong><br>
                 המידע באתר אינו מיועד להנחות את הציבור או לשמש לגביו כהמלצה או הוראה או עצה לשימוש או שינוי או הורדה של תרופה כלשהיא ואין בו תחליף לייעוץ רפואי פרטני או אחר.<br>
הכתוב באתר אינו מהווה המלצה רפואית מוסמכת.
איתי שרף מחזיק בתעודת מטפל מוסמך Dipl Ac.CH והוא איננו רופא MD.
            
             </div>
         </div>
         <div class="hp-form">
             <h4>יצירת קשר</h4>
             <!-- <h5><?php the_field('name','options'); ?></h5>-->
             <p><?php the_field('address','options'); ?></p>
             <p><label>טלפון:</label> <?php the_field('phone','options'); ?><label>פקס:</label> <?php the_field('fax','options'); ?></p>
         <?php echo do_shortcode('[contact-form-7 id="6621" title="hp-form"]'); ?>
         </div>
 </div>
</div>
<div class="hp-sidebar">
<div class="patient-stories">
    <h3><?php the_field('patients-title', 'options'); ?> <a href="<?php the_field('patients-link', 'options'); ?>" class="all">לכל הסרטונים</a></h3>
    <?php $image = wp_get_attachment_image_src( get_field('patients-clip', 'options'), full ); ?>
    <a href="<?php the_field('video-story', 'options'); ?>&autoplay=1&rel=0" class="fancybox-youtube" >
    <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('patients-clip', 'options'))?>" width="331" height="93"/></a>
</div><!-- #patients -->
<div class="patient-stories2">
    <h3>איתי שרף בתקשורת </h3>
        <a href="/wp-content/themes/itaysharf/video/itaysharf.mp4" class="fancybox-youtube" onclick="javascript:ga('send', 'event', 'Video', 'play', 'interview-homepage');">
    <img src="https://www.itaysharf.com/wp-content/uploads/2016/02/video-cover-small2.jpg" alt="איתי שרף בתקשורת" width="331" height="93"></a>
</div><!-- #patients2 -->
<div class="about">
 <h3><?php the_field('about-title', 'options'); ?></h3>
  <?php $image = wp_get_attachment_image_src( get_field('about-image', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('about-image', 'options'))?>" width="331" height="114"/></a>
  <div class="about-content">
  <?php the_field('about-content', 'options'); ?>
  <a href="<?php the_field('about-link', 'options'); ?>" class="more">אודות הקליניקה</a>
  </div>
</div>

</div><!-- #hp-Sidebar -->


			
			</div><!-- #content -->
		</div><!-- #container -->


<?php get_footer(); ?>
