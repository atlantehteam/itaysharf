<?php
/**

 *
 * Template Name: Diseases Page
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
          <div class="featured"><?php
if (has_post_thumbnail()) {
	the_post_thumbnail('page-featured', array('class' => 'page-featured'));
    }  else {
        echo '<img src="'. get_bloginfo('template_url') . '/images/default-wide.jpg" alt="איתי שרף - רפואת עיניים סינית" />';
		
    }?></div>
        
			<div id="content" role="main">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
          <?php 

				// check for rows (parent repeater)
				if( get_field('disease') ): ?>
                 <div class="diseases">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('disease') ): ?>
						<div class="item">
                     <h4 > <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></h4>
                    
<div class="sub-title"><?php the_sub_field('content'); ?> <a href="<?php the_sub_field('link'); ?>" class="more">למידע נוסף</a></div>
                     
                     						 						
						</div>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
                    </div>
									<?php endif; // if( get_field('disease') ): ?>
	
			<?php if( get_field('tabs') ): ?>
                <div id="tabs">
<!-- Using the jquery UI tabs together with ACF plugin. Don't forget to enqeue the jquery ui tabs in the functions.php file -->
	<ul>
            
            <?php while (has_sub_field('tabs') ): $count++ ?>
				<li><a title="<?php the_sub_field('tab_name'); ?>" href="#tab-<?php echo $count ?>"><?php the_sub_field('tab_name'); ?></a><span class="arr"></span></li>
                <?php endwhile; ?>
			<?php endif; ?>
	
	</ul>
	
		<?php if( get_field('tabs') ): ?>
         <?php while (has_sub_field('tabs') ): $count2++ ?>
			<div id="tab-<?php echo $count2 ?>"><?php the_sub_field('tab_content'); ?></div>
		
	<?php endwhile; ?>
    </div>
	<?php endif; ?>



 <div class="inner-form">
 <div class="form-title">יצירת קשר</div>
         <?php echo do_shortcode('[contact-form-7 id="6597" title="new-form"]'); ?>
         </div>
			</div><!-- #content -->
            <?php get_sidebar(); ?>
		</div><!-- #container -->


<?php get_footer(); ?>
