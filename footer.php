<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->

	<div id="footer" role="contentinfo">
		<div id="colophon">
        
          <div class="strip">
        <ul class="features">
         <?php 
 
				// check for rows (parent repeater)
				if( get_field('features', 'options') ): ?>
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('features', 'options') ): ?>
						<li>
                         <?php $image = wp_get_attachment_image_src( get_sub_field('icon', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_sub_field('icon', 'options'))?>" width="56" height="54"/></a>
                     <div class="footer-slogen" > <?php the_sub_field('title', 'options'); ?></div>
                    
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
									<?php endif; // if( get_field('disease') ): ?>
        </ul>
        
        <ul class="logos">
       
         <?php 
 
				// check for rows (parent repeater)
				if( get_field('footer-logos', 'options') ): ?>
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('footer-logos', 'options') ): ?>
						<li>
                         <?php $image = wp_get_attachment_image_src( get_sub_field('logo', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_sub_field('logo', 'options'))?>" height="62"/></a>
                                   
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
									<?php endif; // if( get_field('disease') ): ?>
        
        </ul>
        
        </div>

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

			      <?php 
 
				// check for rows (parent repeater)
				if( get_field('footer-menu','options') ): ?>
					<div class="footer-menu">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('footer-menu','options') ): ?>
						<ul class="section">
							<div class="footer-title"><?php the_sub_field('cat-title'); ?></div>
							<?php 
 
							// check for rows (sub repeater)
							if( get_sub_field('cat-pages','options') ): ?>
								
								<?php 
 
								// loop through rows (sub repeater)
								while( has_sub_field('cat-pages','options') ): 
 
									// display each item as a list - with a class of completed ( if completed )
									?>
									
                                    <li class="item">
                        <a href="<?php the_sub_field('page-link'); ?>" ><?php the_sub_field('page-name'); ?></a>
					
						</li>       
								<?php endwhile; ?>
								
							<?php endif; //if( get_sub_field('items') ): ?>
						</ul>	
 
					<?php endwhile; // while( has_sub_field('staff') ): ?>
                    
                    
                     
                <div class="contact-info">
         <div class="footer-title">יצירת קשר</div>
             <div class="contact-details">
          
             <p><?php the_field('address','options'); ?></p>
             <p><label>טלפון:</label> <?php the_field('phone','options'); ?></p>
             <p><label>מייל:</label><a href="mailto:<?php the_field('email','options'); ?>"><?php the_field('email','options'); ?></a></p>
			<p style="text-decoration:none;"><a href="https://www.facebook.com/itaysharf/">בקרו אותנו בפייסבוק</a></p>
             </div>
         </div>
                    
                    
					</div>
				<?php endif; // if( get_field('staff') ): ?>
               

  <div class="disclaimer">המידע באתר אינו מיועד להנחות את הציבור או לשמש לגביו כהמלצה או הוראה או עצה לשימוש או שינוי או הורדה של תרופה כלשהיא ואין בו תחליף לייעוץ רפואי פרטני או אחר.<br> הכתוב באתר אינו מהווה המלצה רפואית מוסמכת ואינו מבוסס מדעית.<br> איתי שרף מחזיק בתעודת מטפל מוסמך Dipl Ac.CH  והוא איננו רופא MD.</div>
<div class="credit">
  <span class="rights">כל הזכויות שמורות - איתי שרף.</span>
<span class="design">עיצוב: אורן פייט</span><span class="design"><a href="http://www.dmdesign.co.il" target="_blank">בניית אתר: dmdesign</a></span></div>
                
<!--<div class="credit">
<span class="rights">כל הזכויות שמורות למרפאת איתי שרף</span>
<span class="design">עיצוב: אורן פייט</span><span class="design"><a href="http://www.dmdesign.co.il" target="_blank">בניית אתר: dmdesign</a></span>
</div>-->
		</div><!-- #colophon -->
	</div><!-- #footer -->

</div><!-- #wrapper -->
<a href="#0" class="cd-top">Top</a>


<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('a.form-btn').click(function () {

        // Getting the variable's value from a link
        var loginBox = $(this).attr('href');

        //Fade in the Popup and add close button
        $(loginBox).fadeIn(300);

        //Set the center alignment padding + border
       // var popMargTop = ($(loginBox).height() + 24) / 2;
      //  var popMargLeft = ($(loginBox).width() + 24) / 2;

       // $(loginBox).css({
       //     'margin-top': -popMargTop,
       //     'margin-left': -popMargLeft
       // });

        // Add the mask to body
        $('body').append('<div id="mask"></div>');
        $('#mask').fadeIn(300);

        return false;
    });

    // When clicking on the button close or the mask layer the popup closed
    $('body').on('click', 'a.close, #mask', function () {
        $('#mask , .login-popup').fadeOut(300, function () {
            $('#mask').remove();
        });
        return false;
    });
});
</script>

<?php
 $external_link_pages = get_field('external-link-btn-shown-pages', 'options');
 if (!empty( $external_link_pages) && in_array(get_the_ID(), $external_link_pages)) {
	 ?>
<div class="button-strip login-window">
<a href="<?php the_field('external-link-btn-link','options'); ?>" class="form-btn external-link" target="_blank"><?php the_field('external-link-btn-text','options'); ?>
	<button class="form-btn">פתח</button>
</a>
</div>
	 <?php
 } else {
	 ?>
<div class="button-strip login-window">
<a href="#contact-box" class="form-btn"><?php the_field('mobile-form-button','options'); ?></a>
<a href="tel:077-2004961" class="phone-btn"><?php the_field('mobile-phone-button','options'); ?></a>
</div>
	 <?php
 }
?>

<div id="contact-box" class="login-popup">
    <a href="#" class="close">X</a>
    <?php echo do_shortcode ('[contact-form-7 id="4305" html_class="use-floating-validation-tip" title="mobile-popup-form"]') ?> <!-- localhost form id="1043" -->
</div>



<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script>wpcf7.cached = 0;</script>
</body>
</html>
