<?php
/**
 * Template Name: Landing Page
 *
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
          <div class="featured"><?php
if (has_post_thumbnail()) {
	the_post_thumbnail('page-featured', array('class' => 'page-featured'));
    }  else {
        echo '<img src="'. get_bloginfo('template_url') . '/images/default-wide.jpg" alt="איתי שרף - רפואת עיניים סינית" />';
		
    }?></div>
        
			<div id="content" role="main" class="landing">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
                        <?php if(function_exists('fontResizer_place')) { fontResizer_place(); } ?>
					<?php } ?>
                    
                    
                    
                    
                    
                    
                 	<?php if( get_field('landing-heading') ): ?>   <h2><?php the_field('landing-heading'); ?></h2>
                    <?php endif; ?>

					<div class="entry-content">
                    
                    
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
				
					</div><!-- .entry-content -->
                    	 <?php get_sidebar('landing'); ?>
				</div><!-- #post-## -->

		

<?php endwhile; // end of the loop. ?>
        <div class="video_case" id="bigvid">
    <iframe width="705" height="445" src="https://www.youtube.com/embed/<?php the_field('main-video'); ?>&#63rel=0" frameborder="0" allowfullscreen allow="autoplay"></iframe>
  <span class="desc">  <?php the_field('video-desc'); ?></span>
</div>

<?php if (get_field('video-id-1')): ?>
<div class="video_wrapper">
    <ul>
        <li class="video_thumbnail"><a href="https://www.youtube.com/embed/<?php the_field('video-id-1'); ?>&#63rel=0&amp;autoplay=1" class="video-1" title=""><span class="video-btn"></span><img src="https://img.youtube.com/vi/<?php the_field('video-id-1'); ?>/mqdefault.jpg" alt="<?php the_field('video-alt-1'); ?>"/></a></li>
        <li class="video_thumbnail"><a href="https://www.youtube.com/embed/<?php the_field('video-id-2'); ?>&#63rel=0&amp;autoplay=1" class="video-2" title=""><span class="video-btn"></span><img src="https://img.youtube.com/vi/<?php the_field('video-id-2'); ?>/mqdefault.jpg" alt="<?php the_field('video-alt-2'); ?>"/></a></li>
        <li class="video_thumbnail"><a href="https://www.youtube.com/embed/<?php the_field('video-id-3'); ?>&#63rel=0&amp;autoplay=1" class="video-3" title=""><span class="video-btn"></span><img src="https://img.youtube.com/vi/<?php the_field('video-id-3'); ?>/mqdefault.jpg" alt="<?php the_field('video-alt-3'); ?>"/></a></li>
    </ul>
</div>
<?php endif; ?>


<div class="more-content">
<?php the_field('more-content'); ?>
</div>



	
			<?php if( get_field('tabs') ): ?>
                <div id="tabs">
<!-- Using the jquery UI tabs together with ACF plugin. Don't forget to enqeue the jquery ui tabs in the functions.php file -->
	<ul>
            
            <?php while (has_sub_field('tabs') ): $count++ ?>
				<li><h3><a title="<?php the_sub_field('tab_name'); ?>" href="#tab-<?php echo $count ?>"><?php the_sub_field('tab_name'); ?></a><span class="arr"></span></h3></li>
                <?php endwhile; ?>
			<?php endif; ?>
	
	</ul>
	
		<?php if( get_field('tabs') ): ?>
         <?php while (has_sub_field('tabs') ): $count2++ ?>
			<div id="tab-<?php echo $count2 ?>"><?php the_sub_field('tab_content'); ?></div>
		
	<?php endwhile; ?>
    </div>
	<?php endif; ?>


	<div class="more-content landing" style="clear:both; width:100%;">
    
    <?php 
 
				// check for rows (parent repeater)
				if( get_field('more-pages') ): ?>
                <h5><?php the_field('more-pages-title');?></h5>
                <ul class="more-pages">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('more-pages') ): ?>
						<li class="item">
                    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title' ); ?></a>
                    
                     
                     						 						
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
                    </ul>
									<?php endif; // if( get_field('disease') ): ?>
    
    </div>



<div class="content-footer">
<div class="side-img landing hide-desktop"> 
   				 <h5>איתי שרף בתקשורת</h5>
					<a href="/wp-content/themes/itaysharf/video/itaysharf.mp4" class="fancybox-youtube" onclick="javascript:ga('send', 'event', 'Video', 'play', 'interview-landing');">
			         <img src="https://www.itaysharf.com/wp-content/uploads/2016/06/video-cover4.jpg" alt="מרפאת איתי שרף"></a></div>


<div class="book">
 <?php $image = wp_get_attachment_image_src( get_field('book-image', 'options'), full ); ?>
  
  <a href="<?php the_field('book-link', 'options'); ?>" ><img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('book-image', 'options'))?>" width="108" /></a>
 <h3><?php the_field('book-title', 'options'); ?></h3>
 
  <div class="book-content">
  <?php the_field('book-content-landing', 'options'); ?>
 
  </div>
</div>


 <div class="inner-form">
 <div class="form-title">יצירת קשר</div>
         <?php echo do_shortcode('[contact-form-7 id="6597" title="new-form"]'); ?>
         </div>
         
      </div>  <!-- .content footer -->





			</div><!-- #content -->
           
		</div><!-- #container -->


<?php get_footer(); ?>
