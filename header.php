<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' /> 
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T4M8WWC');</script>
<!-- End Google Tag Manager -->
<script id='pixel-script-poptin' src='https://cdn.popt.in/pixel.js?id=3e31526726e78' async='true'></script>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T4M8WWC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
 <script >
jQuery(document).ready(function($){
$("#tabs").tabs();
});

jQuery(document).ready(function($){
$("#tabs-adv").tabs({collapsible:true, active:false});
 $( ".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *" )
.removeClass( "ui-corner-all ui-corner-top" )
.addClass( "ui-corner-bottom" );
// move the nav to the bottom
$( ".tabs-bottom .ui-tabs-nav" ).appendTo( ".tabs-bottom" );
});


</script>

<script type="text/javascript">

// http://www.unitz.com/u-notez/2010/05/how-to-style-animate-wordpress-3-menus/

jQuery(document).ready(function($) {



	$("#header ul.menu li").hover(function() {
	
		$(this).children('ul.sub-menu')
		
			.stop(true, true).animate({ "height": "show", "opacity": "show" }, 200 );
	}, function(){
		$(this).children('ul.sub-menu')
			.stop(true, true).delay(300).animate({ "height": "toggle", "opacity": "toggle" }, 200 );
		
	});
	
});
</script>

<script  > 
jQuery(document).ready(function($){
 
 	//$(".video_wrapper li").click(function() {
//		$('body, html').scrollTo('#bigvid');
//		return false; });


$(".video_wrapper li").click(function() {
    $('html, body').animate({
        scrollTop: $("#bigvid").offset().top
    }, 1000);
});

 $(".video_wrapper").on('click',"a", function(event) {
	
 
	 
  event.preventDefault();
  event.stopImmediatePropagation();
  event.stopPropagation();
 
 $(".video_case iframe").prop("src", $(event.currentTarget).attr("href"));
 


});
});
 </script>
 
 <script >
  jQuery(document).ready(function ($) {
    $(".menu-header li.launcher").mouseover(

    function () {
        $("div.megamenu").fadeIn('fast');
		$(".launcher").addClass('selected');
    });
    $(".menu-header li.launcher, div.megamenu").mouseleave(function () {
        $("div.megamenu").fadeOut('fast');
			$(".launcher").removeClass('selected');
	
    });
    $("div.megamenu").mouseover(function () {
        $(this).stop(true, true).show();
			$(".launcher").addClass('selected');
    });
	
	
	
	
	
	
	 $(".sub-menu-open").click(function () {
        $(".mobile-child-menu").toggle("fast", function(){
			
		});
		
	
	
	
//	 $(".sub-menu-open").click(function () {
//        $(".mobile-child-menu").show('fast');
//		$(".launcher").addClass('selected');
//    });
//    $(".sub-menu-open").click(function () {
//        $(".mobile-child-menu").show('fast');
//			$(".launcher").removeClass('selected');
//	
//    });
//    $(".sub-menu-open").click(function () {
//        $(this).stop(true, true).show();
//			$(".mobile-child-menu").addClass('selected');
//    });
	
	
	
	
	 }); });
	

    </script>

<div id="wrapper" class="hfeed">
	<div id="header">
    	<div id="masthead">
         <!--  <a href="<?php echo bloginfo('url');?>/novel-treatment-now-available-glaucoma-patients" class="eng">English</a> -->
			<div id="branding" role="banner">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'div' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php bloginfo('template_directory'); ?>/images/sharf_logo_new.png" alt="איתי שרף - רפואת עיניים סינית"/></a>
					</span>
				</<?php echo $heading_tag; ?>>
			
            <?php if (is_home()) {?>
            <div class="hp-header-details">
            <img src="<?php bloginfo('template_directory'); ?>/images/phone-img.png" alt="איתי שרף - רפואת עיניים סינית"/>
            </div>
            
            <?php } else { ?>
            <div class="header-details">
            <img src="<?php bloginfo('template_directory'); ?>/images/phone-img.png" alt="איתי שרף - רפואת עיניים סינית"/></div>
                 <?php } ?>
                 
            <!--<div class="header-phone"> <a href="tel:077-2004961">  <img src="<?php bloginfo('template_directory'); ?>/images/phone-img.png" alt="איתי שרף - רפואת עיניים סינית"/></a></div>-->
            
            
             <div class="header-phone"><a href="tel:077-2004961"> טלפון: 077-2004961</a></div>
         

				
			</div><!-- #branding -->

			<div id="access" role="navigation">
			  <?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
				<?php /* Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
				<?php if (!wp_is_mobile()) {wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) );} ?>
                
              
                
                            <?php if ( is_page() || is_archive() ) { ?>

<?php // list child pages of current page
if($post->post_parent)
$children = wp_list_pages('title_li=&child_of='.$post->post_parent.'&echo=0&depth=1&exclude=6061'); else
$children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1&exclude=6061');
if ($children || is_archive() || !is_page(324)) { ?>
  <a href="#" class="sub-menu-open" >הצג דפים נוספים</a>
  <?php } } ?>
  <div class="mobile-child-menu">

<ul>
<?php /*?><?php echo $children; ?><?php */?>

<script >
//  jQuery(document).ready(function ($) {
//
//$(".mobile-child-menu .sub-menu").hide();
//$("mobile-child-menu .current_page_item .sub-menu").show();
// });
 </script> 


</ul>
<?php if (is_page(324) || ($post->post_parent =='324')){?>
<ul>
<?php echo $children; ?>
</ul>
<?php } ?>
<?php
// disabling mobile menu as we have responsive menu
//wp_nav_menu( array( 'container_class' => 'menu-header-mobile', 'theme_location' => 'primary', 'menu_class' => 'mobile-child' ) );
?>
   </div>


                
                
             
                <div class="megamenu">
                
                <div class="mega-items">
                         <?php 
 
				// check for rows (parent repeater)
				if( get_field('mega-items', 'options') ): ?>
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('mega-items', 'options') ): ?>
						<div class="item">
                         <?php $image = wp_get_attachment_image_src( get_sub_field('img'), strip ); ?>
 <a href="<?php the_sub_field('link', 'options'); ?>" target="_top"><img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_sub_field('img'))?>" /></a>
                      	<div class="mega-menu-title"> <a href="<?php the_sub_field('link', 'options'); ?>" target="_top"><?php the_sub_field('title', 'options'); ?></a></div>
						  <?php
							$sub_menu = get_sub_field('submenu', 'options');
							if (!empty($sub_menu)) {
								echo '<ul class="submenu-list">';
								foreach ($sub_menu as $menu_item) {
									$target = empty($menu_item['link']['target']) ? '' : 'target="_blank"';
									echo "<li class='submenu-item'><a href='{$menu_item['link']['url']}' $target>{$menu_item['link']['title']}</a></li>";
								}
								echo '</ul>';
							} else {
								echo '<div class="content">'.get_sub_field('content', 'options').'</div>';
							}
							?>
						</div>	
 
					<?php endwhile; // while( has_sub_field('downloads') ): ?>
					
				<?php endif; // if( get_field('downloads') ): ?>
                        
                         </div>
                
                <div class="more-links">
                <?php $pagelist = get_field('page-rpt', 'options'); 
		if ($pagelist): ?>
       		<ul class="pagelist">
				<?php
                while(has_sub_field('page-rpt', 'options')): ?>
                <?php
               // $p = get_sub_field('page-link', 'options');
                ?>
                <li><a href="<?php the_sub_field('page-link','options'); ?>" ><?php the_sub_field('page-name','options'); ?></a></li>
              <?php /*?> This was the previous code for shoing the page bname and link just by choosing the page from the admin // <li><a href="<?php echo get_permalink($p->ID); ?>" target="_top"><?php echo $p->post_title; ?></a>
                </li><?php */?>
                 
                <?php endwhile; ?>
		</ul>        
		<?php endif; ?>
        <a href="<?php the_field('mega-all-diseases', 'options');?>" target="_top" class="mega-all-diseases">לכל המחלות</a>
                </div>
                </div>
			</div><!-- #access -->
		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main">