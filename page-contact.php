<?php
/**

 ** Template Name: Contact Page

 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" class="contact">
          <div class="featured"><?php
if (has_post_thumbnail()) {
	the_post_thumbnail('page-featured', array('class' => 'page-featured'));
    }  else {
        echo '<img src="'. get_bloginfo('template_url') . '/images/default-wide.jpg" alt="איתי שרף - רפואת עיניים סינית" />';
		
    }?></div>
        
			<div id="content" role="main">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
        
	
			
 <div class="inner-form">
 <div class="form-title">יצירת קשר</div>
         <?php echo do_shortcode('[contact-form-7 id="183" title="contact-page-form"]'); ?>
         </div>
			</div><!-- #content -->
            <?php get_sidebar(); ?>
		</div><!-- #container -->


<?php get_footer(); ?>
