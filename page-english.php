<?php
/**
 ** Template Name: English

 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header('english'); ?>

		<div id="container" style="direction:ltr;">
          <div class="featured"><?php
if (has_post_thumbnail()) {
	the_post_thumbnail('page-featured-eng', array('class' => 'page-featured'));
    }  else {
        echo '<img src="'. get_bloginfo('template_url') . '/images/default-wide.jpg" alt="איתי שרף - רפואת עיניים סינית" />';
		
    }?></div>
        
			<div id="content" role="main">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
        
	
      <?php 

				// check for rows (parent repeater)
				if( get_field('video') ): ?>
                 <div class="video">
					<?php 
 					$counter=1;
					// loop through rows (parent repeater)
					while( has_sub_field('video') ): ?>
						<div class="item">
                     <?php   if( get_sub_field('video-url') ){
  $embed_code = wp_oembed_get( get_sub_field('video-url'), array('height'=>265, 'rel'=>0));
 } ?>                 <div class="vid-container"> <?php echo $embed_code; ?></div>                  
<div class="video-desc"><?php the_sub_field('video-desc'); ?></div>
                                						 						
						</div>	
                        <?php if ($counter % 2 ==0) {echo'<div class="devider"></div>';}
						$counter++; ?>
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
                    </div>
									<?php endif; // if( get_field('disease') ): ?>
    
			<?php if( get_field('tabs') ): ?>
                <div id="tabs">
<!-- Using the jquery UI tabs together with ACF plugin. Don't forget to enqeue the jquery ui tabs in the functions.php file -->
	<ul>
            
            <?php while (has_sub_field('tabs') ): $count++ ?>
				<li><a title="<?php the_sub_field('tab_name'); ?>" href="#tab-<?php echo $count ?>"><?php the_sub_field('tab_name'); ?></a><span class="arr"></span></li>
                <?php endwhile; ?>
			<?php endif; ?>
	
	</ul>
	
		<?php if( get_field('tabs') ): ?>
         <?php while (has_sub_field('tabs') ): $count2++ ?>
			<div id="tab-<?php echo $count2 ?>"><?php the_sub_field('tab_content'); ?></div>
		
	<?php endwhile; ?>
    </div>
	<?php endif; ?>



 <div class="inner-form">
 <h5>Contact Us</h5>
         <?php echo do_shortcode('[contact-form-7 id="191" title="eng-contact"]'); ?>
         </div>
			</div><!-- #content -->
            <?php get_sidebar('english'); ?>
		</div><!-- #container -->


<?php get_footer('eng'); ?>
