<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="xoxo">
            
            
             <?php 
 
				// check for rows (parent repeater)
				if( get_field('more-pages') ): ?>
                <h5><?php the_field('more-pages-title');?></h5>
                <ul class="more-pages">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('more-pages') ): ?>
						<li class="item">
                    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title' ); ?></a>
                    
                     
                     						 						
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
                    </ul>
									<?php endif; // if( get_field('disease') ): ?>
            
            <?php if (!is_page(19)) :?>
            <div class="inner-form">
 <h5>Contact</h5>
         <?php echo do_shortcode('[contact-form-7 id="191" title="eng-contact"]'); ?>
         </div>
         <?php endif ?>
         
  <div class="patient-stories">
    <h3>Success Stories <a href="<?php the_field('patients-link', 'options'); ?>" class="all">See More</a></h3>
    <?php $image = wp_get_attachment_image_src( get_field('patients-clip', 'options'), full ); ?>
    <a href="<?php the_field('video-story'); ?>&autoplay=1&rel=0" >
    <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('patients-clip', 'options'))?>" width="331" height="93"/></a>
</div><!-- #patients -->
<div class="book">
 <h3>Book</h3>
  <?php $image = wp_get_attachment_image_src( get_field('book-image', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_field('book-image', 'options'))?>" width="108" /></a>
  <div class="book-content">
  <?php the_field('book-content', 'options'); ?>
  <a href="<?php the_field('book-link', 'options'); ?>" class="more">לפרטים נוספים ולרכישה</a>
  </div>
</div>





<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

		

		<?php endif; // end primary widget area ?>
			</ul>
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>
