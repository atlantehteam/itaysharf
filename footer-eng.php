<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->

	<div id="footer" role="contentinfo">
		<div id="colophon">
        
          <div class="strip">
        <ul class="features">
         <?php 
 
				// check for rows (parent repeater)
				if( get_field('features-eng') ): ?>
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('features-eng') ): ?>
						<li>
                         <?php $image = wp_get_attachment_image_src( get_sub_field('icon'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_sub_field('icon'))?>" width="56" height="54"/></a>
                     <h4 > <?php the_sub_field('title'); ?></h4>
                    
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
									<?php endif; // if( get_field('disease') ): ?>
        </ul>
        
        <ul class="logos">
       
         <?php 
 
				// check for rows (parent repeater)
				if( get_field('footer-logos', 'options') ): ?>
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('footer-logos', 'options') ): ?>
						<li>
                         <?php $image = wp_get_attachment_image_src( get_sub_field('logo', 'options'), full ); ?>
  <img src="<?php echo $image[0];?>" alt="<?php echo get_the_title(get_sub_field('logo', 'options'))?>" height="62"/></a>
                                   
						</li>	
 					<?php endwhile; // while( has_sub_field('disease') ): ?>
									<?php endif; // if( get_field('disease') ): ?>
        
        </ul>
        
        </div>

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

			      <?php 
 
				// check for rows (parent repeater)
				if( get_field('footer-menu','options') ): ?>
					<div class="footer-menu">
					<?php 
 
					// loop through rows (parent repeater)
					while( has_sub_field('footer-menu','options') ): ?>
						<ul class="section">
							<h5><?php the_sub_field('cat-title'); ?></h5>
							<?php 
 
							// check for rows (sub repeater)
							if( get_sub_field('cat-pages','options') ): ?>
								
								<?php 
 
								// loop through rows (sub repeater)
								while( has_sub_field('cat-pages','options') ): 
 
									// display each item as a list - with a class of completed ( if completed )
									?>
									
                                    <li class="item">
                        <a href="<?php the_sub_field('page-link'); ?>" ><?php the_sub_field('page-name'); ?></a>
					
						</li>       
								<?php endwhile; ?>
								
							<?php endif; //if( get_sub_field('items') ): ?>
						</ul>	
 
					<?php endwhile; // while( has_sub_field('staff') ): ?>
                    
                    
                     
                <div class="contact-info">
         <h5>יצירת קשר</h5>
             <div class="contact-details">
          
             <p><?php the_field('address','options'); ?></p>
             <p><label>טלפון:</label> <?php the_field('phone','options'); ?></p>
             <p><label>מייל:</label><a href="mailto:<?php the_field('email','options'); ?>"><?php the_field('email','options'); ?></a></p>
             </div>
         </div>
                    
                    
					</div>
				<?php endif; // if( get_field('staff') ): ?>
               
                
<div class="credit">
<span class="rights">כל הזכויות שמורות למרפאת איתי שרף</span>
<span class="design">עיצוב: אורן פייט</span>
</div>
		</div><!-- #colophon -->
	</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script>wpcf7.cached = 0;</script>

</body>
</html>
