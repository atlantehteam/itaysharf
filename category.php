<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
         <div class="featured"><?php
if (has_post_thumbnail()) {
	the_post_thumbnail('page-featured', array('class' => 'page-featured'));
    }  else {
        echo '<img src="'. get_bloginfo('template_url') . '/images/default-wide.jpg" alt="איתי שרף - רפואת עיניים סינית" />';
		
    }?></div>
			<div id="content" role="main">

				<h1 class="page-title"><?php single_cat_title( );?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div class="archive-meta">' . $category_description . '</div>';

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

			</div><!-- #content -->
            <?php get_sidebar(); ?>
		</div><!-- #container -->


<?php get_footer(); ?>
